terraform {
  required_providers {
    azurerm = {
        source = "hashicorp/azurerm"
        version = "=3.48.0"

    }
  }
}
provider "azurerm" {
    features{}
}







resource "azurerm_linux_virtual_machine" "pjepri-vm" {
    
    resource_group_name = "pjeprivm_group"
    admin_username = "peter"
    location = "westeurope"
    name = "pjeprivm"
    network_interface_ids = ["/subscriptions/c0177cf4-3641-4a06-bbf6-40d1ac0a60f8/resourceGroups/pjeprivm_group/providers/Microsoft.Network/networkInterfaces/pjeprivm210"]
    os_disk  {
    caching = "ReadWrite"

    disk_size_gb = 30

    name = "pjeprivm_OsDisk_1_b9abee30cc094a78b35d098e9ea49734"

    
    storage_account_type = "Premium_LRS"

    write_accelerator_enabled = false
  }
    source_image_reference {
        offer = "0001-com-ubuntu-server-focal"

        publisher  = "canonical"

        sku  = "20_04-lts-gen2"

        version = "latest"
  }
    admin_ssh_key {
           public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDeCKtCxEQg5vfrcyt5CutK5X5IaO/IVZPPB4NIclJqwAZuZJYBfdECcyGuQ8cYbZklSmN4kC5mkjbKubuQaEckGM80Z5wpv/ZftSjXePitymlHiOg+2erUz6fBzI7YJXqIeXb+8Otya44vjbIo0OeFzNNXqKL90hUlAsfF8SIsLs3LXqwnziBaCnt27jiG+6EdO2w4/x823saiYa/3rtJRxMYLF6BX642MKOk0/qfTpNjJWUn2V/qrFhHVI2VOsohrZAN8ttavPI5xEsVntDIh9TBvelkEP5y4O0ILUbfD1GzdWtUEeJWVj1ZeGBtVA7mw/Kpy6kx+FHy3qdF8BTPwMwKE+vXT5mcPoBO3cd6e61TB8cjEH3TYXxaCzkfrT03ufdoRvh7P6VApNY5qEl75bNGWVdfw72wvgpKVr/V99aAvNa7PEEKYnw9JPShYBMN7rJ1x+GqYEabAAMy/84wTMD4tHkBcjSeKC/YxLc/3F72I0ubVFbKdkZJ7vxJ7AUU= generated-by-azure"
            username = "peter"

    }
    size = "Standard_DS1_v2"
    boot_diagnostics {
                storage_account_uri =  ""
              }
            
 provisioner "local-exec" {
    when    = "create"
    command = "ansible-playbook -i hosts --private-key pjeprivm_key.pem ./playbook.yaml"
  }

    
}




